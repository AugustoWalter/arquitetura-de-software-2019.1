package br.dao;

import java.util.ArrayList;
import java.util.List;

import br.model.Usuario;

public class UsuarioDAO {
	
	public Usuario obtemUsuario(Long id) {
		for (int i = 0; i < (new UsuarioDAO()).obterUsuarios().size(); i++) {
			if(id.equals((new UsuarioDAO()).obterUsuarios().get(i).getId()))
				return (new UsuarioDAO()).obterUsuarios().get(i);
		}
		return null;
	}

	public List<Usuario> obterUsuarios(){
		List<Usuario> usuarios = new ArrayList<Usuario>();
		Usuario user;
		
		user = new Usuario();
		user.setId(1l);
		user.setNome("Mickey");
		user.setLogin("mik");
		user.setPassw("123");
		usuarios.add(user);
		
		user = new Usuario();
		user.setId(2l);
		user.setNome("Juliana");
		user.setLogin("ju");
		user.setPassw("123");
		usuarios.add(user);	
		
		return usuarios;
	}
}
