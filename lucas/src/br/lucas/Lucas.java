package br.lucas;

import br.dao.UsuarioDAO;

public class Lucas {
	
	public int obtemId() {
		return (new UsuarioDAO()).obtemUsuario(2L).getId().intValue();
	}
	
	public String obtemNome() {
		return (new UsuarioDAO()).obtemUsuario(2L).getNome();
	}
	
	public String obtemLogin() {
		return (new UsuarioDAO()).obtemUsuario(2L).getLogin();
	}
}
