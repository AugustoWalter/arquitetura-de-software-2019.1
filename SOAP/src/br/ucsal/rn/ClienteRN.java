package br.ucsal.rn;

import java.util.List;

import javax.jws.WebService;

import br.ucsal.Cliente;
import br.ucsal.BD.ClienteBD;

/*
 * Criaremos agora a classe ClienteRN, que acessar�
 * a ClienteBD para buscar os dados. Esta mesma classe RN
 * servir� depois para expormos o web service.
 */
@WebService
public class ClienteRN {

	private ClienteBD clienteBD;

	public ClienteRN() {
		clienteBD = new ClienteBD();

	}

	public List<Cliente> listar() {
		return clienteBD.listar();
	}

	

}
