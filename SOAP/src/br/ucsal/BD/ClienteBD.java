package br.ucsal.BD;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.Cliente;

/*
 * A Classe ClienteBD representa a camada que faz o
 * acesso aos dados. Criaremos um ArrayList que simular�
 * o acesso a estes dados.
 */
public class ClienteBD {

public List<Cliente> listar(){
	// estamos dando um SELECT abaixo.
	List<Cliente> lista = new ArrayList<>();
    lista.add(new Cliente(100, "Lucas Augusto"));
    lista.add(new Cliente(200, "Haroldo Beyer"));
    return lista;
}


}
